import sharingfilters.filters as f
import sharingfilters.dataset as d
import httpretty
import moto
import boto3
from pathlib import Path
import unittest


@httpretty.activate
@moto.mock_s3
class TestNestedDataset(unittest.TestCase):
    encoder = f.FilterEncoder()

    def test_dataset_full(self):
        ds = d.Dataset("nested-dataset", "http://localhost/catalog", "area1", "token")
        pd = ds.to_pandas()
        assert pd.shape[0] == 6, "full dataset"
        assert len(pd.columns) == 4

    def test_dataset_simple(self):
        ds = d.Dataset("nested-dataset", "http://localhost/catalog", "area1", "token")
        filtered = ds.filter(ds.struct_field.int_field <= -2).select("struct_field", "array_field")
        pd = filtered.to_pandas()
        assert pd.shape[0] == 4, "simple filter"
        assert len(pd.columns) == 2

    def test_dataset_complex(self):
        ds = d.Dataset("nested-dataset", "http://localhost/catalog", "area1", "token")
        filtered = ds.filter(ds.struct_field.int_field <= -2).filter(
            ds.struct_field.str_field.startswith("str3")).select("struct_field", "array_field")
        pd = filtered.to_pandas()
        assert pd.shape[0] == 2, "complex filter"
        assert len(pd.columns) == 2

    def test_dataset_nested_nested(self):
        ds = d.Dataset("nested-dataset", "http://localhost/catalog", "area1", "token")
        filtered = ds.filter(ds.struct_struct_field.struct_field.int_field <= -2).filter(
            ds.struct_struct_field.struct_field.str_field.startswith("str3")).select("struct_field", "array_field", "struct_struct_field")
        pd = filtered.to_pandas()
        assert pd.shape[0] == 2, "complex filter"
        assert len(pd.columns) == 3

    def setUp(self):
        s3 = boto3.client("s3")
        s3.create_bucket(Bucket="bucket")
        s3.put_object(Bucket="bucket", Key="/nested-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/1.parquet",
                      Body="test1")
        s3.put_object(Bucket="bucket", Key="/nested-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/2.parquet",
                      Body="test2")
        url1 = s3.generate_presigned_url(ClientMethod='get_object', Params={'Bucket': 'bucket',
                                                                            'Key': 'nested-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/1.parquet'}).replace(
            "bucket.s3.amazonaws.com", "localhost")
        url2 = s3.generate_presigned_url(ClientMethod='get_object', Params={'Bucket': 'bucket',
                                                                            'Key': 'nested-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/2.parquet'}).replace(
            "bucket.s3.amazonaws.com", "localhost")
        httpretty.register_uri(
            method=httpretty.POST,
            uri="http://localhost/catalog/areas/area1/datasets/nested-dataset/access",
            body=f'''
            {{
             "schema": "{{\\"type\\" : \\"record\\",\\"name\\" : \\"schema\\",\\"fields\\" : [ {{\\"name\\" : \\"struct_field\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"record\\",\\"name\\" : \\"struct_field\\",\\"fields\\" : [ {{\\"name\\" : \\"int_field\\",\\"type\\" : [ \\"null\\", \\"long\\" ],\\"default\\" : null}}, {{\\"name\\" : \\"str_field\\",\\"type\\" : [ \\"null\\", \\"string\\" ],\\"default\\" : null}} ]}} ],\\"default\\" : null}}, {{\\"name\\" : \\"array_field\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"array\\",\\"items\\" : {{\\"type\\" : \\"record\\",\\"name\\" : \\"list\\",\\"fields\\" : [ {{\\"name\\" : \\"item\\",\\"type\\" : [ \\"null\\", \\"long\\" ],\\"default\\" : null}} ]}}}} ],\\"default\\" : null}}, {{\\"name\\" : \\"struct_struct_field\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"record\\",\\"name\\" : \\"struct_struct_field\\",\\"fields\\" : [ {{\\"name\\" : \\"struct_field\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"record\\",\\"name\\" : \\"struct_field\\",\\"namespace\\" : \\"struct_field2\\",\\"fields\\" : [ {{\\"name\\" : \\"int_field\\",\\"type\\" : [ \\"null\\", \\"long\\" ],\\"default\\" : null}}, {{\\"name\\" : \\"str_field\\",\\"type\\" : [ \\"null\\", \\"string\\" ],\\"default\\" : null}} ]}} ],\\"default\\" : null}} ]}} ],\\"default\\" : null}}, {{\\"name\\" : \\"array_struct_field\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"array\\",\\"items\\" : {{\\"type\\" : \\"record\\",\\"name\\" : \\"list\\",\\"namespace\\" : \\"list2\\",\\"fields\\" : [ {{\\"name\\" : \\"item\\",\\"type\\" : [ \\"null\\", {{\\"type\\" : \\"record\\",\\"name\\" : \\"item\\",\\"namespace\\" : \\"\\",\\"fields\\" : [ {{\\"name\\" : \\"int_field\\",\\"type\\" : [ \\"null\\", \\"long\\" ],\\"default\\" : null}}, {{\\"name\\" : \\"str_field\\",\\"type\\" : [ \\"null\\", \\"string\\" ],\\"default\\" : null}} ]}} ],\\"default\\" : null}} ]}}}} ],\\"default\\" : null}}, {{\\"name\\" : \\"__index_level_0__\\",\\"type\\" : [ \\"null\\", \\"string\\" ],\\"default\\" : null}} ]}}",
             "partitionSchema" : "another useless schema",
             "datasetName": "nested-dataset",
             "datasource": "datasource.net",
             "partitions": [
                {{
                  "partitionValues" : [2020, 3, 17, "EMEA"],
                  "files": [
                    {{"path": "{url1}", "size": 100}},              
                    {{"path": "{url2}", "size": 200}}             
                  ]
                }}
             ]
            }}
            '''
        )
        content = Path("resources/nested.parquet").read_bytes()
        httpretty.register_uri(method=httpretty.GET, uri=url1, body=content)
        httpretty.register_uri(method=httpretty.GET, uri=url2, body=content)
