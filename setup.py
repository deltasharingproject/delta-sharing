from setuptools import find_packages, setup

setup(
    name='sharingfilters',
    packages=find_packages(),
    install_requires=["pyarrow", "pandas", "requests", "json", "fsspec", "s3fs", "avro"],
    version='0.1.0',
    description='Python filters for delta sharing',
    author='Anton Dorozhkin',
    license='MIT',
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
)