import json

import sharingfilters.filters as f
from sharingfilters.utils import column_op
import requests
import pyarrow.dataset as dataset
from urllib.parse import urlparse
import fsspec
from functools import reduce
import avro.schema as avs


class Column:
    def __init__(self, name, parents=[]):
        self.name = name
        self.parents = parents

    # logistic operators
    __eq__ = column_op(f.EqualsTo)
    __ne__ = column_op(f.NotEqualsTo)
    __lt__ = column_op(f.LessThan)
    __le__ = column_op(f.LessThanOrEquals)
    __ge__ = column_op(f.GreateThanOrEquals)
    __gt__ = column_op(f.GreateThan)

    eqNullSafe = column_op(f.EqualsToNullSafe)
    contains = column_op(f.Contains)
    startswith = column_op(f.StartsWith)
    endswith = column_op(f.EndsWith)

    def isin(self, values):
        return f.In(self.name, values)

    def __getattr__(self, name):
        return Column(name, self.parents + [self.name])

    def get_full_name(self):
        return '.'.join(self.parents + [self.name])


class AccessPlanRequest:
    def __init__(self, filters):
        self.filters = filters


class Dataset:
    def __init__(self, name, base_url, area, token, filters=[], columns=None):
        self.name = name
        self.baseUrl = base_url
        self.area = area
        self.token = token
        self.filters = filters
        self.encoder = f.FilterEncoder()
        self.columns = columns

    def __getattr__(self, name):
        return Column(name)

    def filter(self, dataset_filter):
        return Dataset(self.name, self.baseUrl, self.area, self.token, self.filters + [dataset_filter], self.columns)

    def select(self, *columns):
        return Dataset(self.name, self.baseUrl, self.area, self.token, self.filters, list(columns))

    def to_pandas(self):
        response = requests.post(url=f'{self.baseUrl}/areas/{self.area}/datasets/{self.name}/access',
                                 json=self.encoder.encode(AccessPlanRequest(self.filters)),
                                 headers={"Authorization": self.token}
                                 )
        json_body = response.json()
        schema = avs.parse(json_body["schema"])
        returned_filters = [f.from_dict(flt) for flt in json.loads(json_body["returnedFilters"])] if "returnedFilters" in json_body else self.filters

        files = [file["path"] for partition in json_body["partitions"] for file in partition["files"]]
        url = urlparse(files[0])
        protocol = url.scheme
        filesystem = fsspec.filesystem(protocol)
        ds = dataset.dataset(source=files, format="parquet", filesystem=filesystem)

        filters = returned_filters[0].to_pyarrow(schema) if len(returned_filters) == 1 else None
        filters = reduce(lambda a, b: f.And(a, b), returned_filters).to_pyarrow(schema) if len(returned_filters) > 1 else filters

        return ds.to_table(columns=self.columns, filter=filters).to_pandas()
