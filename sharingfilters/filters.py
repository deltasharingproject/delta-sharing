import json

from avro.schema import UnionSchema, RecordSchema
from pyarrow._compute import MatchSubstringOptions, StructFieldOptions
from sharingfilters.utils import logical_op, unary_op
from pyarrow import dataset as ds
from functools import reduce


def _bin_op(ctor):
    def _(self, other):
        result = ctor(self.name, other)
        return result

    return _


def _not_op():
    def _(self):
        return Not(None)

    return _


class Filter:
    references = []

    def __init__(self, references):
        self.references = references

    def to_pyarrow(self, schema):
        pass

    def get_field(self, schema):
        tokens = self.name.split(".")
        if len(tokens) == 1:
            return ds.field(self.name)
        else:
            head, *tail = tokens
            tuple = reduce(lambda a, b:\
                           (a[0]._call("struct_field", [a[0]], StructFieldOptions([self.get_field_index(b, a[1])])), self.get_field_schema(b, a[1])), \
                           tail, \
                           (ds.field(head), schema.fields_dict[head]) \
                           )
            return tuple[0]

    def get_field_index(self, name, struct):
        if isinstance(struct.type, UnionSchema):
            return [i for i,j in enumerate(next(filter(lambda f: isinstance(f, RecordSchema), struct.type.schemas)).fields) if j.name==name][0]
        else:
            if isinstance(struct.type, RecordSchema):
                return [i for i,j in enumerate(struct.type.fields) if j.name==name][0]
            else:
                return -1

    def get_field_schema(self, name, struct):
        if isinstance(struct.type, UnionSchema):
            return next(filter(lambda f: isinstance(f, RecordSchema), struct.type.schemas)).fields_dict[name]
        else:
            if isinstance(struct.type, RecordSchema):
                return struct.type.fields_dict[name]
            else:
                return None


def findReferences(obj):
    if (isinstance(obj, Filter)):
        return obj.references
    else:
        return []


def singleValueReferences(name, value):
    return [name] + findReferences(value)


def multiValueRefenrences(name, values):
    result = [name]
    for v in values:
        result += findReferences(v)
    return result


class FilterEncoder(json.JSONEncoder):
    def default(self, obj):
        return obj.__dict__


class And(Filter):
    def __init__(self, left, right):
        super().__init__(findReferences(left) + findReferences(right))
        self.left = left
        self.right = right
        setattr(self, "@type", "and")

    def to_pyarrow(self, schema):
        return self.left.to_pyarrow(schema) & self.right.to_pyarrow(schema)


class Or(Filter):
    def __init__(self, left, right):
        super().__init__(findReferences(left) + findReferences(right))
        self.left = left
        self.right = right
        setattr(self, "@type", "or")

    def to_pyarrow(self, schema):
        return self.left.to_pyarrow(schema) | self.right.to_pyarrow(schema)


class EqualsTo(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "equal-to")

    def to_pyarrow(self, schema):
        return self.get_field(schema) == self.value


class EqualsToNullSafe(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "equal-to-null-safe")

    def to_pyarrow(self, schema):
        # TODO check me
        return self.get_field(schema) == self.value


class NotEqualsTo(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "not-equal-to")

    def to_pyarrow(self, schema):
        return self.get_field(schema) != self.value


class GreateThan(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "greater-than")

    def to_pyarrow(self, schema):
        return self.get_field(schema) > self.value


class GreateThanOrEquals(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "greater-than-or-equals")

    def to_pyarrow(self, schema):
        return self.get_field(schema) >= self.value


class LessThan(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "less-than")

    def to_pyarrow(self, schema):
        return self.get_field(schema) < self.value


class LessThanOrEquals(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "less-than-or-equals")

    def to_pyarrow(self, schema):
        return self.get_field( schema) <= self.value


class In(Filter):
    def __init__(self, name, values):
        super().__init__(multiValueRefenrences(name, values))
        self.name = name
        self.values = values
        setattr(self, "@type", "in")

    def to_pyarrow(self, schema):
        return self.get_field(schema).isin(self.values)


class Not(Filter):
    def __init__(self, child):
        super().__init__(findReferences(child))
        self.child = child
        setattr(self, "@type", "not")

    def to_pyarrow(self, schema):
        return ~(self.child.to_pyarrow(schema))


class StartsWith(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "string-starts-with")

    def to_pyarrow(self, schema):
        return self.get_field(schema)._call("starts_with", [self.get_field(schema)], MatchSubstringOptions(self.value))


class EndsWith(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "string-ends-with")

    def to_pyarrow(self, schema):
        return self.get_field(schema)._call("ends_with", [self.get_field(schema)], MatchSubstringOptions(self.value))


class Contains(Filter):
    def __init__(self, name, value):
        super().__init__(singleValueReferences(name, value))
        self.name = name
        self.value = value
        setattr(self, "@type", "string-contains")

    def to_pyarrow(self, schema):
        return self.get_field(schema)._call("match_substring", [self.get_field(schema)],
                                            MatchSubstringOptions(self.value))


def from_dict(dict):
    if "@type" not in dict:
        return dict
    type_ = dict["@type"]
    if type_ == "and":
        return And(from_dict(dict["left"]), from_dict(dict["right"]))
    if type_ == "or":
        return Or(from_dict(dict["left"]), from_dict(dict["right"]))
    if type_ == "equal-to":
        return EqualsTo(dict["name"], dict["value"])
    if type_ == "equal-to-null-safe":
        return EqualsToNullSafe(dict["name"], dict["value"])
    if type_ == "not-equal-to":
        return NotEqualsTo(dict["name"], dict["value"])
    if type_ == "greater-than":
        return GreateThan(dict["name"], dict["value"])
    if type_ == "greater-than-or-equals":
        return GreateThanOrEquals(dict["name"], dict["value"])
    if type_ == "less-than":
        return LessThan(dict["name"], dict["value"])
    if type_ == "less-than-or-equals":
        return LessThanOrEquals(dict["name"], dict["value"])
    if type_ == "in":
        return In(dict["name"], dict["values"])
    if type_ == "not":
        return Not(from_dict(dict["child"]))
    if type_ == "string-starts-with":
        return StartsWith(dict["name"], dict["value"])
    if type_ == "string-ends-with":
        return EndsWith(dict["name"], dict["value"])
    if type_ == "string-contains":
        return Contains(dict["name"], dict["value"])

    return dict


Filter.__and__ = logical_op(And)
Filter.__or__ = logical_op(Or)
Filter.__invert__ = unary_op(Not)
Filter.__rand__ = logical_op(And)
Filter.__ror__ = logical_op(Or)
