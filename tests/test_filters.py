import json

import sharingfilters.filters as f

encoder = f.FilterEncoder()

def test_serializer():
  andFilter = f.And(f.EqualsTo("name1", 1), f.EqualsTo("name2", "2"))
  assert encoder.encode(andFilter) == '{"references": ["name1", "name2"], "left": {"references": ["name1"], "name": "name1", "value": 1, "@type": "equal-to"}, "right": {"references": ["name2"], "name": "name2", "value": "2", "@type": "equal-to"}, "@type": "and"}'

  orFilter = f.Or(f.EqualsTo("name1", 1), f.EqualsTo("name2", "2"))
  assert encoder.encode(orFilter) == '{"references": ["name1", "name2"], "left": {"references": ["name1"], "name": "name1", "value": 1, "@type": "equal-to"}, "right": {"references": ["name2"], "name": "name2", "value": "2", "@type": "equal-to"}, "@type": "or"}'

  inFilter = f.In("name1", [1,2,3])
  assert encoder.encode(inFilter)=='{"references": ["name1"], "name": "name1", "values": [1, 2, 3], "@type": "in"}'

  notFilter = f.Not(inFilter)
  assert encoder.encode(notFilter) == '{"references": ["name1"], "child": {"references": ["name1"], "name": "name1", "values": [1, 2, 3], "@type": "in"}, "@type": "not"}'

  decodedFilter = f.from_dict(json.loads(
    '{"references": ["name1", "name2"], "left": {"references": ["name1"], "name": "name1", "value": 1, "@type": "equal-to"}, "right": {"references": ["name2"], "name": "name2", "value": "2", "@type": "equal-to"}, "@type": "and"}'))

  assert isinstance(decodedFilter, f.And)
  assert isinstance(decodedFilter.left, f.EqualsTo)
