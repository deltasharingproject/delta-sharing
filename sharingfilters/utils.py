
def column_op(ctor):
    def _(self, other):
        return ctor(self.get_full_name(), other)
    return _


def logical_op(ctor):
    def _(self, other):
        return ctor(self, other)
    return _


def unary_op(ctor):
    def _(self):
        return ctor(self)
    return _
