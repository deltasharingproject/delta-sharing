import sharingfilters.filters as f
import sharingfilters.dataset as d
import httpretty
import moto
import boto3
from pathlib import Path
import unittest

@httpretty.activate
@moto.mock_s3
class TestDatasetWithReturnedFilters(unittest.TestCase):
    encoder = f.FilterEncoder()


    def test_dataset_returned_filters(self):
        ds = d.Dataset("some-dataset", "http://localhost/catalog", "area1", "token")
        filtered = ds.filter(ds.id <= 10).filter(ds.first_name.startswith("A")).select("id", "first_name", "last_name", "email")
        pd = filtered.to_pandas()
        assert pd.shape[0] == 10, "complex filter"


    def setUp(self):
        s3 = boto3.client("s3")
        s3.create_bucket(Bucket="bucket")
        s3.put_object(Bucket="bucket", Key="/some-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/1.parquet",
                      Body="test1")
        url1 = s3.generate_presigned_url(ClientMethod='get_object', Params={'Bucket': 'bucket',
                                                                            'Key': 'some-dataset.parquet/YEAR=2020/MONTH=3/DAY=17/REGION=EMEA/1.parquet'}).replace(
            "bucket.s3.amazonaws.com", "localhost")
        httpretty.register_uri(
            method=httpretty.POST,
            uri="http://localhost/catalog/areas/area1/datasets/some-dataset/access",
            body=f'''
            {{
             "schema": "{{  \\"type\\" : \\"record\\",  \\"name\\" : \\"hive_schema\\",  \\"fields\\" : [ {{    \\"name\\" : \\"registration_dttm\\",    \\"type\\" : [ \\"null\\", {{      \\"type\\" : \\"fixed\\",      \\"name\\" : \\"INT96\\",      \\"doc\\" : \\"INT96 represented as byte[12]\\",      \\"size\\" : 12    }} ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"id\\",    \\"type\\" : [ \\"null\\", \\"int\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"first_name\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"last_name\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"email\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"gender\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"ip_address\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"cc\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"country\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"birthdate\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"salary\\",    \\"type\\" : [ \\"null\\", \\"double\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"title\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }}, {{    \\"name\\" : \\"comments\\",    \\"type\\" : [ \\"null\\", \\"string\\" ],    \\"default\\" : null  }} ]}}             ",
             "partitionSchema" : "another useless schema",
             "returnedFilters": "[{{\\"references\\": [\\"id\\"], \\"name\\": \\"id\\", \\"value\\": 10, \\"@type\\": \\"less-than-or-equals\\"}}]",
             "datasetName": "some-dataset",
             "datasource": "datasource.net",
             "partitions": [
                {{
                  "partitionValues" : [2020, 3, 17, "EMEA"],
                  "files": [
                    {{"path": "{url1}", "size": 100}}              
                  ]
                }}
             ]
            }}
            '''
        )
        content = Path("resources/userdata1.parquet").read_bytes()
        httpretty.register_uri(method=httpretty.GET, uri=url1, body=content)