import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd
import numpy as np

df = pd.DataFrame({'struct_field': [{'int_field':-1, "str_field":"str1"}, {'int_field':-2, "str_field":"str2"} ,{'int_field':-3, "str_field":"str3"}],
                   'array_field': [[1,2,3], [4,5,6], [7,8,9]],
                   # 'map_field': [map("key", "value1"),map("key", "value2"), map("key", "value3")],
                   'struct_struct_field': [{"struct_field": {'int_field':-1, "str_field":"str1"}}, {"struct_field": {'int_field':-2, "str_field":"str2"}}, {"struct_field": {'int_field':-3, "str_field":"str3"}}],
                   'array_struct_field': [[{'int_field':-1, "str_field":"str1"}], [{'int_field':-2, "str_field":"str2"}], [{'int_field':-3, "str_field":"str3"}]]},
                   index=list('abc'))


table = pa.Table.from_pandas(df)
pq.write_table(table, '../tests/resources/nested.parquet')