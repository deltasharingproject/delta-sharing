import sharingfilters.filters as f
import sharingfilters.dataset as d
import json

encoder = f.FilterEncoder()

def test_dataset_filter():
    ds = d.Dataset("some-dataset", "http://somehost.org/catalog", "area1", "token")
    assert encoder.encode(ds.a == "123") == '{"references": ["a"], "name": "a", "value": "123", "@type": "equal-to"}'

    assert encoder.encode((ds.a == "123") & (ds.b >= 3)) == '{"references": ["a", "b"], "left": {"references": ["a"], "name": "a", "value": "123", "@type": "equal-to"}, "right": {"references": ["b"], "name": "b", "value": 3, "@type": "greater-than-or-equals"}, "@type": "and"}'

    assert encoder.encode(~(ds.a == "123")) == '{"references": ["a"], "child": {"references": ["a"], "name": "a", "value": "123", "@type": "equal-to"}, "@type": "not"}'

    assert encoder.encode(ds.a.isin([1, 2, 3])) == '{"references": ["a"], "name": "a", "values": [1, 2, 3], "@type": "in"}'

    assert encoder.encode(ds.a.startswith("abc")) == '{"references": ["a"], "name": "a", "value": "abc", "@type": "string-starts-with"}'

    filtered = ds.filter(ds.a == "123").filter((ds.b >= 3))
    assert len(filtered.filters) == 2
